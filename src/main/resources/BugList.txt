
Number		: 2
Date		: Jan. 2011
Reporter	: mh
Description	: 
	will man in der konsole das resultat einer summenberechnung in folgender weise skalieren
		sum( ((* Modules.Organ *)).getVolume() )*5
	so erhaelt man eine de.grogra.util.WrapException, die auch nicht mehr verschwindet. man muss das modell schließen und wieder neue oeffnen


Number		: 3
Date		: Mai 2007
Reporter	: mh
Description	: 

	* globale variable und lokal instanziiert. problem type ist verschieden (IF und instanz)
	* der kompiler schlukt es, speichern geht, man kann das modell nur nicht wieder oeffnen 
	import java.util.ArrayList;
	import java.util.List;

	List a;

	protected void init () [
		{
		    a = new ArrayList();
		}
	]


Number		: 4
Date		: Nov. 2010
Reporter	: mh
Description	: 
	public float getA() {
			return 42;
			//
			;
		}
	Unexpected Exception ClassFormatError: Invalid pc in LineNumberTable in class file OrgansS1$Root  Stack Trace: java.lang.ClassFormatError: Invalid pc in LineNumberTable in class file OrgansS1$Root      at java.lang.ClassLoader.defineClass1(Native Method)
	wird der kommentar "//" entfernt laeuft es durch


Number		: 6 - beantwortet von Ole (18.10.2012; email an mh)
Date		: 
Reporter	: kata, mh
Description	: parameter von modulen, die selbst auch konten sind, weden im graph gefunden
module B(Node x);
module C;

public void init() [
	Axiom ==> B(new C());
]

Ole: "Solche Parameter werden als eigenständige Knoten-Objekte behandelt und in der internen Knoten-Liste abgespeichert. 
Eine Suchabfrage wie c:C startet nun nicht beim Wurzelknoten und sucht von da aus alle Vorkommen von C, sondern geht über 
die Knoten-Liste. Das ist wesentlich effizienter, zumal die Knoten-Liste in Unterlisten entsprechend der Klassen-
hierarchie aufgeteilt ist. D.h. es muss nur über die Liste aller C-Knoten iteriert werden. Andererseits werden so auch 
Knoten gefunden, die nicht an der Wurzel hängen.

Eine Lösung wäre, module B(Object x) zu verwenden. Dann wird x nicht als eigenständiges Knoten-Objekt behandelt, selbst 
wenn man x einen Knoten zuweist."

public void run() [
	c:C ::> println("gefunden in ::>");
	c:C ==> println("gefunden in ==>");
	c:C ==>> println("gefunden in ==>>");
]

Ausgabe:
> gefunden in ::>
gefunden in ==>

Ole: ""gefunden in ==>>" kommt nicht, da c ja schon zuvor bei ==> gefunden wurde und diese Regel den Knoten aus dem 
Graphen aushängen würde (wenn er denn drin wäre), somit wird er für die folgenden Regeln als gelöscht markiert und nicht 
mehr gefunden."

C wird im graph gefunden, obwohl es nicht dort sein duerfe und laut 2d graph anzeige auch nicht ist.
mit einer graph-abfrage wird C ebenfalls gefunden.  

> (* Model.C *)
de.grogra.turtle.C[id=11]@5b83d60d

des weiteren wird der parameter x (vom type node von B) nicht im attribut editor angezeigt

Ole: "Die Anzeige geht hier nicht, da ja der x-Parameter als eigenständiger Knoten aufgefasst wird und somit letztlich 
eine Art Knoten-Auswahlfeld angezeigt werden müsste, mit dem man einen beliebigen Knoten aus dem Graphen herauspicken 
könnte. So ein Auswahlfeld gibt es aber nicht. Mit dem (Object x)-Trick wird der Parameter angezeigt.

Es wäre möglich, eine Annotation einzuführen, die sagt, dass der Parameter doch kein eigenständiges Objekt sein soll, 
etwa module B(@SecondClassObject Node x). @SecondClassObject wäre mein Vorschlagr wegen der Unterscheidung First-Class-
Object (eigene Identität) und Second-Class-Object (keine eigene Identität, nur der Wert ist relevant), siehe 
http://en.wikipedia.org/wiki/First-class_citizen."


Number		: 7
Date		: schon immer
Reporter	: mh
Description	: probleme mit dem entfernen von bildern und dateien aus dem File Explorer bzw. Images Panel;
	- löschen von bildern aus dem images panel bewirkt nur ein loeschen aus der liste .. die bilder bleiben im porjekt erhalten; was dazu fuehrt, dass wenn man ein neues bild mit dem nahmen des bereits geleoschten bildes einfuegt der name um eine "2" ergenzt wird
	- löschen von dateien aus dem Fiel Explorer bewirkt auch nur ein löschen aus der liste und nicht aus dem projekt (ein erneutes einfuegen der zuvor gelöschten datei funktioniert)
	- das mehrfache einfuegen einer datei mit selben namen fuer zu einem NullPointerException
Unexpected Exception 
NullPointerException  
Stack Trace: java.lang.NullPointerException      
at de.grogra.pf.registry.Registry.getProjectFile(Registry.java:372)      
at de.grogra.pf.ui.registry.SourceFile.activateImpl(SourceFile.java:155)      
at de.grogra.pf.registry.Item$1.visit(Item.java:913)      
at de.grogra.pf.registry.Item.deriveItems(Item.java:847)      
at de.grogra.pf.registry.Item.activate(Item.java:926)      
at de.grogra.pf.registry.Registry.activateItems(Registry.java:784)      
at de.grogra.pf.registry.Registry.commitXA(Registry.java:518)      
at de.grogra.pf.registry.Item.commitXA(Item.java:1145)      
at de.grogra.pf.registry.Item.add(Item.java:1185)      
at de.grogra.pf.registry.Item.add(Item.java:1164)      
at de.grogra.pf.registry.Item.addWithUniqueName(Item.java:1172)      
at de.grogra.pf.registry.Item.addUserItemWithUniqueName(Item.java:1158)      
at de.grogra.pf.ui.registry.ExplorerMenuBuilder$1ExplorerMenu.run(ExplorerMenuBuilder.java:105)      
at de.grogra.pf.ui.UI$1Task.run(UI.java:565)      
at de.grogra.graph.impl.GraphManager.invokeRun(GraphManager.java:311)      
at de.grogra.util.LockableImpl.invokeRun0(Unknown Source)      
at de.grogra.util.LockableImpl.executeImpl(Unknown Source)      
at de.grogra.util.LockableImpl.execute(Unknown Source)      
at de.grogra.pf.ui.UI.executeLockedly(UI.java:612)      
at de.grogra.pf.ui.UI.executeLockedly(UI.java:619)      
at de.grogra.pf.ui.registry.ExplorerMenuBuilder$1ExplorerMenu.eventOccured(ExplorerMenuBuilder.java:85)      
at de.grogra.pf.ui.tree.UITreePipeline$Node.handleEvent(UITreePipeline.java:170)      
at de.grogra.pf.ui.tree.UITreePipeline.eventOccured(UITreePipeline.java:637)      
at de.grogra.pf.ui.awt.ButtonSupport$Dispatcher.run(ButtonSupport.java:118)      
at de.grogra.imp.IMPJobManager.run(IMPJobManager.java:550)      
at java.lang.Thread.run(Thread.java:662)



Number		: 8
Date		: 28.02.2012
Reporter	: mh
Description	: nicht initialisierte variable hängt GroIMP auf

Dieser code:
protected void init () {
	int i;
	i = i + 1;
	println("i = "+i);
}

erzeugt: 
Unexpected Exception 
VerifyError: (class: Model, method: init signature: ()V) Accessing value from uninitialized register 1  
Stack Trace: 
java.lang.VerifyError: (class: Model, method: init signature: ()V) Accessing value from uninitialized register 1      
at java.lang.Class.getDeclaredMethods0(Native Method)      
at java.lang.Class.privateGetDeclaredMethods(Class.java:2427)      
at java.lang.Class.getDeclaredMethods(Class.java:1791)      
at de.grogra.reflect.ClassAdapter.getDeclaredMethods(Unknown Source)      
at de.grogra.reflect.ClassAdapter.getDeclaredMethodCount(Unknown Source)      
at de.grogra.reflect.Reflection.findMethodWithPrefixInTypes(Unknown Source)      
at de.grogra.pf.registry.TypeItem.deactivateImpl(TypeItem.java:133)      
at de.grogra.pf.registry.Item.deactivate(Item.java:935)      
at de.grogra.pf.registry.Item.removeDerivedItems(Item.java:897)      
at de.grogra.pf.registry.Item.deactivate(Item.java:937)      
at de.grogra.pf.ui.registry.SourceFile$1Deactivator.visit(SourceFile.java:398)      
at de.grogra.pf.registry.Item.forAll(Item.java:1324)      
at de.grogra.pf.registry.Item.forAll(Item.java:1342)      
at de.grogra.pf.ui.registry.SourceFile$1Deactivator.runImpl(SourceFile.java:378)      
at de.grogra.pf.ui.util.LockProtectedCommand$1.run(LockProtectedCommand.java:52)      
at de.grogra.pf.ui.UI$1Task.run(UI.java:565)      
at de.grogra.graph.impl.GraphManager.invokeRun(GraphManager.java:311)      
at de.grogra.util.LockableImpl.invokeRun0(Unknown Source)      
at de.grogra.util.LockableImpl.executeImpl(Unknown Source)      
at de.grogra.util.LockableImpl.execute(Unknown Source)       
...
 
ein nachträgliches initialisieren der variable lässt den fehler nicht(!) verschwinden.
ein vergleichbares java programm  
public class test {
	public static void main(String[] args) {
		int i;
		i=i+1;
		System.out.println("i= "+i);
		System.exit(0);
	}
}
erzeugt beim kompilieren
> javac test.java 
test.java:6: variable i might not have been initialized
		i=i+1;
		  ^
1 error


Number		: 9
Date		: 20.04.2012
Reporter	: mh
Description	: umbenennen von dateien im file explorer funktioniert nicht 
- wird nach dem enter wieder auf den urspruenglichen namen zurueck gesetzt 


Number		: 10
Date		: 20.04.2012
Reporter	: gbs, mh
Description	: setShaders und SideSwitchShader wird im 3d-view (openGL) nicht unterstuetzt

beide seiten bleiben rot (analog bei leaf())
protected void init ()
[
        Axiom ==> 
                Parallelogram().(setShaders(new RGBAShader(1,0,0), new RGBAShader(0,1,0)))
                M(1)
                Parallelogram().(setShader(new SideSwitchShader(new RGBAShader(1,0,0), new RGBAShader(0,1,0))));
]

mit einer nurbssurface geht es auch nicht
protected void init ()
[
        Axiom ==> NURBSSurface(new BezierSurface(new float[] {
                        0, 0, 0, 1, 0, 0, 2, 0, 0, 3, 0, 0,
                        0, 1, 0, 1, 1, 5, 2, 1, -5, 3, 1, 0,
                        0, 2, 0, 1, 2, -5, 2, 2, 5, 3, 2, 0,
                        0, 3, 0, 1, 3, 0, 2, 3, 0, 3, 3, 0
                }, 3, 4)).(setShaders(new RGBAShader(1,0,0), new RGBAShader(0,1,0)), 
                        setFlatness(0.001), setVisibleSides(Attributes.VISIBLE_SIDES_BOTH));
]

und mit nem patch auch nicht
protected void init () [
        {
                // height field
                NetworkHeightField hf = new NetworkHeightField();
                // resoluton of the height field grid
                hf.setXSize(6);
                hf.setYSize(10);
                // instantiate a patch with the height field
                Patch p = new Patch(hf);
                p.setShader(new SideSwitchShader(BLUE, RED));
                //p.setShaders(BLUE, RED);
        }
        Axiom ==> p;
]




Number		: 13
Date		: 02.05.2012
Reporter	: mh
Description	: einfuegen eines polygons verursacht fehler (und toetet 3d-view)
wenn man versucht (so in seiner naivitaet) diesen weg "Object -> Polygonal -> Polygon" im code nachzubauen und einfach nur das:

protected void init () [
	Axiom ==> Polygon;
]

schreibt, erzeugt es das:
Exception in thread "ViewThread@de.grogra.imp3d.gl.GLDisplay@366aa95b" javax.media.opengl.GLException: java.lang.reflect.InvocationTargetException
	at javax.media.opengl.GLJPanel.display(GLJPanel.java:259)
	at de.grogra.imp3d.gl.GLDisplay$1.run(GLDisplay.java:422)
	at de.grogra.graph.impl.GraphManager.invokeRun(GraphManager.java:319)
	at de.grogra.util.LockableImpl.invokeRun0(LockableImpl.java:562)
	at de.grogra.util.LockableImpl.executeImpl(LockableImpl.java:308)
	at de.grogra.util.LockableImpl.executeForcedly(LockableImpl.java:438)
	at de.grogra.util.Utils.executeForcedlyAndUninterruptibly(Utils.java:1770)
	at de.grogra.imp3d.gl.GLDisplay.invokeRender(GLDisplay.java:404)
	at de.grogra.imp.awt.ViewComponentAdapter.run(ViewComponentAdapter.java:322)
	at java.lang.Thread.run(Thread.java:662)
Caused by: java.lang.reflect.InvocationTargetException
	at java.awt.EventQueue.invokeAndWait(EventQueue.java:1042)
	at javax.media.opengl.GLJPanel.display(GLJPanel.java:257)
	... 9 more
Caused by: java.lang.NullPointerException
	at de.grogra.imp3d.gl.GLDisplay.drawPolygons(GLDisplay.java:2944)
	at de.grogra.imp3d.objects.Polygon.draw(Polygon.java:361)
	at de.grogra.imp3d.gl.GLDisplay$GLVisitor.visitImpl(GLDisplay.java:702)
	at de.grogra.imp3d.DisplayVisitor.visitEnterImpl(DisplayVisitor.java:98)
	at de.grogra.imp3d.Visitor3D.visitEnter(Visitor3D.java:150)
	at de.grogra.imp3d.Visitor3D.visitEnter(Visitor3D.java:116)
	at de.grogra.graph.impl.GraphManager.accept0(GraphManager.java:1554)
	at de.grogra.graph.impl.GraphManager.accept(GraphManager.java:1680)
	at de.grogra.imp3d.gl.GLDisplay.render(GLDisplay.java:1228)
	at de.grogra.imp.awt.ViewComponentAdapter.invokeRenderSync(ViewComponentAdapter.java:565)
	at de.grogra.imp3d.gl.GLDisplay.access$7(GLDisplay.java:1)
	at de.grogra.imp3d.gl.GLDisplay$2.run(GLDisplay.java:457)
	at de.grogra.graph.impl.GraphManager.invokeRun(GraphManager.java:319)
	at de.grogra.util.LockableImpl.invokeRun0(LockableImpl.java:562)
	at de.grogra.util.LockableImpl.executeImpl(LockableImpl.java:308)
	at de.grogra.util.LockableImpl.executeForcedly(LockableImpl.java:460)
	at de.grogra.util.Utils.executeForcedlyAndUninterruptibly(Utils.java:1793)
	at de.grogra.imp3d.gl.GLDisplay.display(GLDisplay.java:445)
	at com.sun.opengl.impl.GLDrawableHelper.display(GLDrawableHelper.java:78)
	at javax.media.opengl.GLJPanel$Updater.display(GLJPanel.java:1056)
	at com.sun.opengl.impl.GLDrawableHelper.display(GLDrawableHelper.java:78)
	at com.sun.opengl.impl.GLPbufferImpl$DisplayAction.run(GLPbufferImpl.java:222)
	at com.sun.opengl.impl.GLDrawableHelper.invokeGL(GLDrawableHelper.java:194)
	at com.sun.opengl.impl.GLPbufferImpl.maybeDoSingleThreadedWorkaround(GLPbufferImpl.java:208)
	at com.sun.opengl.impl.GLPbufferImpl.display(GLPbufferImpl.java:88)
	at javax.media.opengl.GLJPanel.paintComponent(GLJPanel.java:659)
	at javax.swing.JComponent.paint(JComponent.java:1029)
	at javax.swing.JComponent.paintToOffscreen(JComponent.java:5124)
	at javax.swing.BufferStrategyPaintManager.paint(BufferStrategyPaintManager.java:278)
	at javax.swing.RepaintManager.paint(RepaintManager.java:1224)
	at javax.swing.JComponent._paintImmediately(JComponent.java:5072)
	at javax.swing.JComponent.paintImmediately(JComponent.java:4882)
	at javax.media.opengl.GLJPanel$PaintImmediatelyAction.run(GLJPanel.java:1228)
	at java.awt.event.InvocationEvent.dispatch(InvocationEvent.java:199)
	at java.awt.EventQueue.dispatchEventImpl(EventQueue.java:641)
	at java.awt.EventQueue.access$000(EventQueue.java:84)
	at java.awt.EventQueue$1.run(EventQueue.java:602)
	at java.awt.EventQueue$1.run(EventQueue.java:600)
	at java.security.AccessController.doPrivileged(Native Method)
	at java.security.AccessControlContext$1.doIntersectionPrivilege(AccessControlContext.java:87)
	at java.awt.EventQueue.dispatchEvent(EventQueue.java:611)
	at java.awt.EventDispatchThread.pumpOneEventForFilters(EventDispatchThread.java:269)
	at java.awt.EventDispatchThread.pumpEventsForFilter(EventDispatchThread.java:184)
	at java.awt.EventDispatchThread.pumpEventsForHierarchy(EventDispatchThread.java:174)
	at java.awt.EventDispatchThread.pumpEvents(EventDispatchThread.java:169)
	at java.awt.EventDispatchThread.pumpEvents(EventDispatchThread.java:161)
	at java.awt.EventDispatchThread.run(EventDispatchThread.java:122)


Number		: 14
Date		: 02.05.2012
Reporter	: kata
Description	: fehlender compilerfehler 

das  
public void run () {
 double a = (* A *).len;
 println(a);
}

liefer richtier weise den fehler 

Found one error. pfs:Model.rgg 11:13 - 11:17 : 
    11.  double a = (* A *).len;
                    ^--^
*** Semantic error: This iterator expression is not properly enclosed by an iteration target.

da das sum(...) vergessen wurde. schribt man nun aber das bsp. nur etwas anders 
public void run () {
 double a;
 a = (* A *).len;
 println(a);
}

gibt es keine fehlermeldung mehr.


Number		: 15 (see problem 25)
Date		: 24.05.2012
Reporter	: tsu-wei
Description	: probleme, wenn rgg datei und ein modul den gleichen namen tragen 

angenommen in einem groeßeren projekt gibt es eine Organ.rgg datei, in dieser alle pflanzenorgane definiert sind. 
nun hat man vielleicht die absicht fuer alle oprgane eine einheitliche superklasse anzulegen und alle konkreten organe von
dieser superklassse erben zu lassen. 

Organ.rgg:

module Organ {
	int age = 0;
	...
	public voild update(float tempsum) {
		age++;
		this.tempsum += tempsum;
	}
}

in einer anderen datei, wo die regeln verwaltet werden hat man nun eine regel, um alle organe pro schritt ein mal zu updaten. 
	x:Organ ::> {
		x.update(42);
	}


nach dem speichern bekommt man aber dies:

Found one error. pfs:Rule.rgg 59:17 - 59:28 : 
    59.                 x.update(42);
                        ^---------^
*** Semantic error: No method named update was found in type Organ.

es wird also in der Organ.rgg nach einer methode update gesucht und nicht im module. 
es werden also dateinamen in regeln akzeptiert und der compiler erlaubt sogar 

Rule ==> Organ;   


Number		: 16
Date		: 15.05.2012
Reporter	: lifeng
Description	: Problem beim rendern mit twilight von objekten bei denen die größe null ist. 
Bsp.: parallelogramm mit breite null wird nach dem rendern als quader von geschätzt 10m seitenlaenge angezeigt.

protected void init ()
[
	Axiom ==> Parallelogram(1,0).(setShader(GREEN));
]

sphere und box machen keine probleme. weitere nicht untersucht.


Number		: 17
Date		: 18.07.2012
Reporter	: mh
Description	: anzeige von 3D-CS objekten mit openGL proteus verursacht fehler:

Reading file Model.rgg ...
Done
File Model.rgg was read.
Exception in thread "ViewThread@de.grogra.imp3d.glsl.GLSLDisplay@28fe53cf" javax.media.opengl.GLException: java.lang.reflect.InvocationTargetException
	at javax.media.opengl.GLJPanel.display(GLJPanel.java:259)
	at de.grogra.imp3d.glsl.GLDisplay$1.run(GLDisplay.java:409)
	at de.grogra.graph.impl.GraphManager.invokeRun(GraphManager.java:311)
	at de.grogra.util.LockableImpl.invokeRun0(Unknown Source)
	at de.grogra.util.LockableImpl.executeImpl(Unknown Source)
	at de.grogra.util.LockableImpl.executeForcedly(Unknown Source)
	at de.grogra.util.Utils.executeForcedlyAndUninterruptibly(Unknown Source)
	at de.grogra.imp3d.glsl.GLDisplay.invokeRender(GLDisplay.java:391)
	at de.grogra.imp.awt.ViewComponentAdapter.run(ViewComponentAdapter.java:318)
	at java.lang.Thread.run(Thread.java:662)
Caused by: java.lang.reflect.InvocationTargetException
	at java.awt.EventQueue.invokeAndWait(EventQueue.java:1042)
	at javax.media.opengl.GLJPanel.display(GLJPanel.java:257)
	... 9 more
Caused by: java.lang.NullPointerException
	at de.grogra.graph.impl.Edge.setObjectMark(Edge.java:370)
	at de.grogra.graph.impl.GraphManager$2.putObject(GraphManager.java:1375)
	at de.grogra.graph.ObjectTreeAttribute.getDerived(ObjectTreeAttribute.java:198)
	at de.grogra.imp3d.objects.GlobalTransformation.get(GlobalTransformation.java:151)
	at de.grogra.imp3d.objects.Sequence.calculateCache(Sequence.java:188)
	at de.grogra.imp3d.objects.Sequence.getCache(Sequence.java:170)
	at de.grogra.imp3d.objects.VertexSequence.writeStamp(Unknown Source)
	at de.grogra.math.ProfileSweep.writeStamp(Unknown Source)
	at de.grogra.math.SkinnedSurface.writeStamp(Unknown Source)
	at de.grogra.graph.Cache$Entry.getValue(Unknown Source)
	at de.grogra.graph.Cache.getValue(Unknown Source)
	at de.grogra.imp3d.PolygonizationCache.get(Unknown Source)
	at de.grogra.imp3d.glsl.GLSLDisplay.drawPolygons(GLSLDisplay.java:699)
	at de.grogra.imp3d.glsl.GLSLDisplay.drawPolygons(GLSLDisplay.java:662)
	at de.grogra.imp3d.objects.NURBSSurface.draw(NURBSSurface.java:262)
	at de.grogra.imp3d.glsl.renderable.GLSLNullRenderable.draw(Unknown Source)
	at de.grogra.imp3d.glsl.renderable.GLSLRenderable.drawAlt(Unknown Source)
	at de.grogra.imp3d.glsl.utility.Drawable.draw(Drawable.java:215)
	at de.grogra.imp3d.glsl.utility.Drawable.draw(Drawable.java:248)
	at de.grogra.imp3d.glsl.renderpass.RenderPass.renderVector(RenderPass.java:45)
	at de.grogra.imp3d.glsl.renderpass.CacheScenePass.render(CacheScenePass.java:134)
	at de.grogra.imp3d.glsl.renderpass.RenderPass.process(RenderPass.java:299)
	at de.grogra.imp3d.glsl.renderpass.FullRenderPass.process(Unknown Source)
	at de.grogra.imp3d.glsl.renderpass.FullQualityRenderPass.render(FullQualityRenderPass.java:94)
	at de.grogra.imp3d.glsl.renderpass.RenderPass.process(RenderPass.java:299)
	at de.grogra.imp3d.glsl.GLSLDisplay.renderScene(GLSLDisplay.java:1362)
	at de.grogra.imp3d.glsl.GLSLDisplay.render(GLSLDisplay.java:387)
	at de.grogra.imp.awt.ViewComponentAdapter.invokeRenderSync(ViewComponentAdapter.java:561)
	at de.grogra.imp3d.glsl.GLDisplay.access$7(GLDisplay.java:1)
	at de.grogra.imp3d.glsl.GLDisplay$2.run(GLDisplay.java:444)
	at de.grogra.graph.impl.GraphManager.invokeRun(GraphManager.java:311)
	at de.grogra.util.LockableImpl.invokeRun0(Unknown Source)
	at de.grogra.util.LockableImpl.executeImpl(Unknown Source)
	at de.grogra.util.LockableImpl.executeForcedly(Unknown Source)
	at de.grogra.util.Utils.executeForcedlyAndUninterruptibly(Unknown Source)
	at de.grogra.imp3d.glsl.GLDisplay.display(GLDisplay.java:432)
	at com.sun.opengl.impl.GLDrawableHelper.display(GLDrawableHelper.java:78)
	at javax.media.opengl.GLJPanel$Updater.display(GLJPanel.java:1056)
	at com.sun.opengl.impl.GLDrawableHelper.display(GLDrawableHelper.java:78)
	at com.sun.opengl.impl.GLPbufferImpl$DisplayAction.run(GLPbufferImpl.java:222)
	at com.sun.opengl.impl.GLDrawableHelper.invokeGL(GLDrawableHelper.java:194)
	at com.sun.opengl.impl.GLPbufferImpl.maybeDoSingleThreadedWorkaround(GLPbufferImpl.java:208)
	at com.sun.opengl.impl.GLPbufferImpl.display(GLPbufferImpl.java:88)
	at javax.media.opengl.GLJPanel.paintComponent(GLJPanel.java:659)
	at javax.swing.JComponent.paint(JComponent.java:1029)
	at javax.swing.JComponent.paintToOffscreen(JComponent.java:5124)
	at javax.swing.BufferStrategyPaintManager.paint(BufferStrategyPaintManager.java:278)
	at javax.swing.RepaintManager.paint(RepaintManager.java:1224)
	at javax.swing.JComponent._paintImmediately(JComponent.java:5072)
	at javax.swing.JComponent.paintImmediately(JComponent.java:4882)
	at javax.media.opengl.GLJPanel$PaintImmediatelyAction.run(GLJPanel.java:1228)
	at java.awt.event.InvocationEvent.dispatch(InvocationEvent.java:199)
	at java.awt.EventQueue.dispatchEventImpl(EventQueue.java:641)
	at java.awt.EventQueue.access$000(EventQueue.java:84)
	at java.awt.EventQueue$1.run(EventQueue.java:602)
	at java.awt.EventQueue$1.run(EventQueue.java:600)
	at java.security.AccessController.doPrivileged(Native Method)
	at java.security.AccessControlContext$1.doIntersectionPrivilege(AccessControlContext.java:87)
	at java.awt.EventQueue.dispatchEvent(EventQueue.java:611)
	at java.awt.EventDispatchThread.pumpOneEventForFilters(EventDispatchThread.java:269)
	at java.awt.EventDispatchThread.pumpEventsForFilter(EventDispatchThread.java:184)
	at java.awt.EventDispatchThread.pumpEventsForHierarchy(EventDispatchThread.java:174)
	at java.awt.EventDispatchThread.pumpEvents(EventDispatchThread.java:169)
	at java.awt.EventDispatchThread.pumpEvents(EventDispatchThread.java:161)
	at java.awt.EventDispatchThread.run(EventDispatchThread.java:122)

import de.grogra.blocks.*;
import static de.grogra.blocks.BlockConst.*;

protected void init ()
[
	Axiom ==> PhiBall(1000) -MULTIPLY-> Sphere(0.02);
]



Number		: 18
Date		: 07.09.2012
Reporter	: wk, bzw. einer der studenten
Description	: kein fehler aber problematisch / irreführend
- der konstruktor von Parallelogram hat zwei parameter (float length, float width)
wenn man nun versucht auf width zuzugreifen, beispielsweise mit A(super.length, super.width) extends Parallelogram
oder spaeter mit x:Parallelogram ::> {x[width] = 42; } bekommt man einen nullpointer.
der leider auch "richtig" ist, da width nicht, wie der konstruktor suggeriert und wie man es von anderen 
primitiven her gewohnt ist, keine globale variable ist, sondern nur lokal im konstruktor existiert.

anmerkung von winfried: bei einem parallelogramm sollte man den winkel zwischen den beiden seiten angeben koennen, 
sonst waere es doch immer nur ein rechteck 



Number		: 19 - beantworte von ole (18.10.2012; email an mh)
Date		: 08.10.2012
Reporter	: kata
Description	: "magische" verdopplung von knoten;
wir das model gespeichert, erzeugt es eine ausgabe mit einem knoten - wie zu erwarten.
drueckt man anschließend reset erhaelt man in der ausgabe zwei(!) knoten.

Ole: "Das liegt daran, dass der A-Knoten in tmp gespeichert wird. Zunächst mal ist das derselbe Knoten wie der im Graphen,
 so dass auch nur einer gefunden wird. "Reset" löscht aber nur den Graphen und nicht die sonstigen Variablen, so dass dann
 zum Zeitpunkt von "a::A ::> println(a);" der neu erzeugte Knoten sowie der noch in tmp gespeicherte alte Knoten existieren
  und gefunden werden.

Auch hier hilft wieder der Trick (s. problem 6) mit Object (private Object tmp;), da dann das Ablegen des Knotens in tmp 
nicht dafür sorgt, dass der Knoten dadurch in der Knoten-Liste gehalten wird. Alternativ geht an dieser Stelle auch 
"private transient A tmp;", was ebenfalls dafür sorgt, dass der Inhalt von tmp nicht in der Knoten-Liste gehalten wird, 
und zusätzlich noch, dass der Inhalt auch nicht als Teil des Projektes mit abgespeichert wird."

auch ein abfragen in der console liefert zwei knoten. im 2d graphen hingegen wird nur ein(!) knoten angezeigt. 

module A extends Sphere(0.1).(setShader(GREEN));

private A tmp;

protected void init () {
	[
		Axiom ==> A;
	]

	{
		derive();
		println();
	}
	
	[
		a:A ::> println(a);
	]

	tmp = first((* A *));	// problem
}

kommentiert man die "problemzeile" aus, funktioniert alles wie erwartet.
die init methode als graph-methode umschreiben erzeugt das selbe verhalten.

module A extends Sphere(0.1).(setShader(GREEN));

private A tmp;

protected void init () [
		Axiom ==> A;

	{
		derive();
		println();
	}
	
	a:A ::> println(a);
	
	{
		tmp = first((* A *));	
	}
]


Number		: 20
Date		: 21.11.2012
Reporter	: mh
Description	: ClassCastException, sobald man mit der maus im 3d view das object geht

beispiel: technics/grs.gsz aus der galerie

Unexpected Exception 
ClassCastException: de.grogra.imp3d.objects.GRSMesh cannot be cast to de.grogra.imp3d.objects.PolygonMesh  
Stack Trace: 
java.lang.ClassCastException: de.grogra.imp3d.objects.GRSMesh cannot be cast to de.grogra.imp3d.objects.PolygonMesh
     at de.grogra.imp3d.objects.MeshNode.pick(MeshNode.java:162)
     at de.grogra.imp3d.PickRayVisitor.visitEnterImpl(PickRayVisitor.java:90)
     at de.grogra.imp3d.Visitor3D.visitEnter(Visitor3D.java:150)
     at de.grogra.imp3d.Visitor3D.visitEnter(Visitor3D.java:116)
     at de.grogra.graph.impl.GraphManager.accept0(GraphManager.java:1611)
...



Number		: 21
Date		: 20.01.2013
Reporter	: wk
Description	: virtual laserscanner verarbeitet objekte mit transparenz nicht richtig

bps. leaf objekt mit teilweise transparenter textur wird nach dem scannen als recheckige flaeche dargestellt
(git sicherlich auch fuer andere primitive mit transparenten texturen)



Number		: 22
Date		: 06.02.2013
Reporter	: mh
Description	: führendes minuszeichen verursacht sematischen fehler. 
in der konsole: 
location((* Sphere *))).z<-1

das minuszeichen fuer es zu diesem fehler:
Found one error.
console 1:1 - 1:48 :
     1. location((* Sphere *))).z<-1
        ^--------------------------^
*** Semantic error: Operator <- cannot be applied to double, int.

"<-" wird also als operator angesehen. es funktioniert nur mit klammern "(-1)" 

und auch im XL code:
private boolean isLocationOk() {
	A a = first((* A *));
	return location(a).x<-1;
}



Number		: 23
Date		: 03.07.2013
Reporter	: wk
Description	:  das
module A(super.length) extends Cone(length, radius);
verursacht diese meldung: 
WARNING: Unexpected Exception
java.lang.VerifyError: (class: Model$A, method: <init> signature: (F)V) Expecting to find object/array on stack
	at java.lang.Class.getDeclaredMethods0(Native Method)
	at java.lang.Class.privateGetDeclaredMethods(Class.java:2521)
	at java.lang.Class.getDeclaredMethods(Class.java:1845)
	at de.grogra.reflect.ClassAdapter.getDeclaredMethods(ClassAdapter.java:892)
	at de.grogra.reflect.ClassAdapter.getDeclaredMethodCount(ClassAdapter.java:912)
	at de.grogra.reflect.Reflection.findMethodWithPrefixInTypes(Reflection.java:678)
	at de.grogra.pf.registry.TypeItem.activateImpl(TypeItem.java:112)
	at de.grogra.pf.registry.Item$1.visit(Item.java:913)
	at de.grogra.pf.registry.Item.deriveItems(Item.java:847)
	at de.grogra.pf.registry.Item.activate(Item.java:926)
	at de.grogra.pf.registry.Registry.activateItems(Registry.java:811)
	at de.grogra.pf.ui.registry.SourceFile$1Deactivator.runImpl(SourceFile.java:746)
	at de.grogra.pf.ui.util.LockProtectedCommand$1.run(LockProtectedCommand.java:52)
	at de.grogra.pf.ui.UI$1Task.run(UI.java:565)
	at de.grogra.graph.impl.GraphManager.invokeRun(GraphManager.java:323)
	at de.grogra.util.LockableImpl.invokeRun0(LockableImpl.java:562)
	at de.grogra.util.LockableImpl.executeImpl(LockableImpl.java:308)
	at de.grogra.util.LockableImpl.execute(LockableImpl.java:272)
	at de.grogra.pf.ui.UI.executeLockedly(UI.java:612)
	at de.grogra.pf.ui.util.LockProtectedCommand.run(LockProtectedCommand.java:42)
	at de.grogra.imp.IMPJobManager.run(IMPJobManager.java:550)
	at java.lang.Thread.run(Thread.java:724)

Problem: nur ein parameter in A definiert (wird radius hinzugefuegt laeuft es). 
gleiche problem mit cylinder. der fehler toetet groimp. ein schließen des projekts ist notwendig
 
Number		: 24
Date		: 01.02.2014
Reporter	: mh
Description	: groimp installer does not find java during installation on win 8
	and also just take one if several java versions are installed; 
	better would be a list of installed java version, where the user can choose which version should be used

Number		: 25 (see problem 15) 
Date		: 2015-06-03
Reporter	: junqi
Description	: model does not work: rule can not be applied; problem is a name conflict: when the variable 'RH' is deleted it works

int RH = 1;

module A(float len) extends Sphere(0.1).(setShader(GREEN));

protected void init () [
    Axiom ==> RH(90) A(1);
]

public void run () [
    RH A(x) ==> F(x) [RU(30) RH(90) A(x*0.8)] [RU(-30) RH(90) A(x*0.8)];
]

a warning concernign the name conflict would be helpful. same problematic for many other turle commends.


Number		: 26
Date		: 2015-06-11
Reporter	: PdV
Description	: analog zu problem 25; name conflict between the name of a variab and a module; unfortunately, there is no warning


const int A = 0;

module A;

protected void init () [
	Axiom ==> A;
]

when excecuted A will be an IntNode.


Number		: 27
Date		: 2015-11-17
Reporter	: mh
Description	: visualization of spectral light curve at attribute editor:
for a LightNode with SpectralLight the visualization of BlackBody works but not for ConstantSPD,
but for a PhongShader its the opposite



Number		: 28
Date		: Sep 2016
Reporter	: MH
Description	: calling a not available function caused "No method named TextLabel was found"; same with Ledgend object

protected void init ()
[
	Axiom ==> TextLabel("test").setShader(GREEN);
]


pfs:Model.rgg could not be opened. 
Found one error. 
pfs:Model.rgg 3:19 - 3:35 : 
     3.         Axiom ==> TextLabel("test").setShader(GREEN);
                          ^--------------^
*** Semantic error: No method named TextLabel was found.



Number		: 29
Date		: Sep 2016
Reporter	: MH
Description	: two semikolon cause strange error message (actually the second semicolon should be ignored, or?)


protected void init ()
[
	Axiom ==> TextLabel("test");;
]

pfs:Model.rgg could not be opened. 
Found one error. 
pfs:Model.rgg 3:37 - 3:38 : 
     3.         Axiom ==> TextLabel("test");;
                                            ^
*** Syntactic error: "]" expected instead of this token. 



Number		: 30
Date		: 2020-06-39
Reporter	: c bahr
Description	: java function call within a right hand side of a rule without java section. code works but how/why?! 

// produces arbitrary plant-like structure
public void goCorrect() {
	clearConsole();
	enableView3DRepaint();
	//disableView3DRepaint();
	for(int i=0; i<repetitions; i++){ // loop at correct position
	[
		s:S ==> 		
		RL(random(-10,10)) RH(random(-180,180)) T(s.number) 
		[RL(-10) S(s.number)]
		[RL(10) S(s.number)]
		derive()		//why does this here work?
		;
	]
	}
}



Number		: 32
Date		: 20201015
Reporter	: mh
Description	: missing sysntax error

protected void init () [
	float a;
	Axiom ==> Sphere();
]

liefert diese warnung:

pfs:Model.rgg could not be opened. ArrayIndexOutOfBoundsException: Index -1 out of bounds for length 1  
Stack Trace: 
java.lang.ArrayIndexOutOfBoundsException: Index -1 out of bounds for length 1      
at de.grogra.xl.expr.GetQuery.complete(GetQuery.java:111)      
at de.grogra.xl.compiler.scope.MethodScope.complete(MethodScope.java:756)      
at de.grogra.xl.compiler.scope.MethodScope.complete(MethodScope.java:762)      
at de.grogra.xl.compiler.scope.MethodScope.complete(MethodScope.java:762)      
at de.grogra.xl.compiler.scope.MethodScope.complete(MethodScope.java:762)      
at de.grogra.xl.compiler.scope.MethodScope.complete(MethodScope.java:762)      
at de.grogra.xl.compiler.scope.MethodScope.complete(MethodScope.java:762)      
at de.grogra.xl.compiler.scope.MethodScope.complete(MethodScope.java:762)      
at de.grogra.xl.compiler.scope.MethodScope.complete(MethodScope.java:762)      
at de.grogra.xl.compiler.scope.MethodScope.complete(MethodScope.java:762)      
at de.grogra.xl.compiler.scope.MethodScope.complete(MethodScope.java:762)      
at de.grogra.xl.compiler.scope.MethodScope.complete(MethodScope.java:737)      
at de.grogra.xl.compiler.CompilerBase.compileMethod(CompilerBase.java:4695)      
at de.grogra.xl.compiler.CompilerBase.finish(CompilerBase.java:4684)      
at de.grogra.xl.compiler.Compiler.classDecl(Compiler.java:1080)      
at de.grogra.xl.compiler.Compiler.compilationUnit(Compiler.java:215)      
at de.grogra.xl.compiler.Compiler.compile(Compiler.java:74)      
at de.grogra.xl.compiler.CompilerBase.compile(CompilerBase.java:286)      
at de.grogra.rgg.model.CompilationFilter.compile(CompilationFilter.java:144)      
at de.grogra.rgg.model.CompilationFilter.loadResource(CompilationFilter.java:263)      
at de.grogra.pf.ui.registry.SourceFile.activateImpl(SourceFile.java:551)      
at de.grogra.pf.registry.Item$1.visit(Item.java:966)      
at de.grogra.pf.registry.Item.deriveItems(Item.java:899)      
at de.grogra.pf.registry.Item.activate(Item.java:979)      
at de.grogra.pf.registry.Registry.activateItems(Registry.java:811)      
at de.grogra.pf.ui.registry.SourceFile$1Deactivator.runImpl(SourceFile.java:746)      
at de.grogra.pf.ui.util.LockProtectedCommand$1.run(LockProtectedCommand.java:52)      
at de.grogra.pf.ui.UI$1Task.run(UI.java:570)      
at de.grogra.graph.impl.GraphManager.invokeRun(GraphManager.java:335)      
at de.grogra.util.LockableImpl.invokeRun0(LockableImpl.java:562)      
at de.grogra.util.LockableImpl.executeImpl(LockableImpl.java:308)      
at de.grogra.util.LockableImpl.execute(LockableImpl.java:272)      
at de.grogra.pf.ui.UI.executeLockedly(UI.java:617)      
at de.grogra.pf.ui.util.LockProtectedCommand.run(LockProtectedCommand.java:42)      
at de.grogra.imp.IMPJobManager.run(IMPJobManager.java:550)      
at java.base/java.lang.Thread.run(Thread.java:834)

stat einer syntaxwarnung, dass "float a" is "an unxepected token" 



Number		: 31
Date		: 
Reporter	: 
Description	: 


* reset globaler varis 


* im sequenziellen mode wird java code in einer regel immer ausgefuerhrt, auch wenn die regel nicht angewendet wird


