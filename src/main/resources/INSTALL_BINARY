GroIMP 1.5


Growth-Grammar-Related Interactive Modelling Platform -- binary distribution
----------------------------------------------------------------------------

Since you are reading this file, you have already downloaded and unpacked the
GroIMP binary distribution. Your installation directory includes GroIMP's
core classes and resource files together with a number of plugins in the
"plugins"-subdirectory. The core files and plugins collectively comprise a
basic GroIMP installation including support for, e.g., 3D-modelling and
rule-based modelling with XL / Relational Growth Grammars.


Requirements
------------

GroIMP has been tested on Unix, Windows and MacOS platforms. As a Java
application, it should follow Java's WORA-principle (Write Once, Run Anywhere)
and, thus, should run on every Java-capable platform (at least in theory).

GroIMP 1.5 requires a Java installation of version 1.6 or later.


Running GroIMP
--------------

The binary distribution contains the executable jar-file "core.jar" in
its installation directory. If your system is set up suitably, GroIMP
can be started by just clicking on the jar-file. Otherwise, you have to
enter the command

java -jar path/to/core.jar

at the command line, where of course path/to/core.jar has to point
from the current working directory to "core.jar". This command requires
a properly installed Java runtime environment.

Possibly, the specification of command line options may enhance the runtime
behaviour. See the documentation of your Java installation for possible
options. E.g., for the Java runtime environment of Sun, the option -Xmx
specifies the maximum amount of heap memory size that should be allocated.
The default value of this option may be much smaller than your installed
main memory size, in this case GroIMP cannot benefit from your memory
until you specify the -Xmx option as in

java -Xmx400m -jar path/to/core.jar

which allows a maximum heap memory allocation of 400 MB.


Adding plugins
--------------

GroIMP can be extended by plugins. For the binary distribution, the
easiest way to make a plugin available is its installation as a directory
tree in the "plugins"-directory so that the file "plugin.xml" is contained
immediately in the subdirectory of the "plugins"-directory:

root-directory of binary distribution
|-- core.jar
|-- <additional files>
`-- plugins
    |-- Plugin 1
    |   |-- plugin.xml
    |   `-- <additional files>
    |-- Plugin 2
    |   ...
    ...

See also the installation notes of the plugin.

However, using the "-p" or (synonymous) "--user-plugins" option, you may
specify additional locations of plugins and directories containing
plugins. E.g.:

java -jar path/to/core.jar -p path/to/plugin1:path/to/plugin2

java -jar path/to/core.jar --user-plugins=path/to/plugindir

Every directory in the specified path list and its immediate subdirectories
are scanned for "plugin.xml"-files which signal the presence of a plugin.


Restrictions on distribution
----------------------------

This release of the GroIMP distribution is covered by the GNU General Public
License as described by the following copyright notice.

GroIMP Extensible, Interactive Modelling Platform
Copyright (C) 2002-2008 Lehrstuhl Grafische Systeme, BTU Cottbus
Copyright (C) 2008-2013 GroIMP Developer Team

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License can be found in the file
LICENSE, included in this distribution.


Third party content
-------------------

Some plugins contain products of third parties. See the
"Help/About GroIMP"-dialog of GroIMP and license-files.
