package de.grogra.build;


import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.project.MavenProject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;




@Mojo(name = "enhance-src", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public class Plugin extends AbstractMojo {

	@Component
	MavenProject mavenProject;
  
	private ArrayList fileSets = new ArrayList ();
	private File stamp, tmp;

  
	public Iterator<File> getFiles() {
		File dir = new File(mavenProject.getBasedir().toString() + "/src/main/java");
		if( !dir.isDirectory()) {
			getLog().info(dir.toString() + " is not a directory. Quit enhancement.");
			return Collections.emptyIterator();
		}
		String[] extensions = new String[] {".java"};
		IOFileFilter filter = new SuffixFileFilter(extensions, IOCase.INSENSITIVE);
		Iterator iter = FileUtils.iterateFiles(dir, filter, TrueFileFilter.INSTANCE);
		List<File> l = new ArrayList<File>();
		
		while (iter.hasNext()){
			File f = (File) iter.next();
			try ( Scanner scanner = new Scanner(f) ) {
				while (scanner.hasNextLine()){
					if (scanner.nextLine().contains("//enh:") ){
						l.add(f);
						break;
					}
				}
			}
			catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		return l.iterator();
	}


	private void enhance(File file) throws MojoExecutionException {
		if (stamp != null)
		{
			if (file.lastModified () <= stamp.lastModified ())
			{
				return;
			}
		}
		try
		{
			InputStream plFile = getClass ().getClassLoader ()
				.getResourceAsStream ("enhance.pl");
			if (plFile == null)
			{
				throw new MojoExecutionException ("enhance.pl not found");
			}
			Process p = Runtime.getRuntime ().exec
				(new String[] {"perl", "-w", "-", file.toString (), tmp.toString ()});
			OutputStream perlIn = p.getOutputStream ();
			int i;
			while ((i = plFile.read ()) >= 0)
			{
				perlIn.write (i);
			}
			perlIn.flush ();
			perlIn.close ();
			// display generated error messages
			InputStream perlErr = p.getErrorStream();
			while (perlErr.available() >0)
			{
				System.err.print((char)perlErr.read());
			}
			perlErr.close ();
			
			p.waitFor ();
			if (p.exitValue () != 0)
			{
				throw new MojoExecutionException
					("perl returned exit value " + p.exitValue ()
					 + ", command was perl -w - " + file + " " + tmp);
			}
			String s = file.getName ();
			if (!s.endsWith (".java"))
			{
				file = new File (file.getParentFile (),
								 s.substring (0, s.lastIndexOf ('.'))
								 + ".java");
			}
			BufferedReader in;
			String old;
			StringBuffer b;
			if (file.exists ())
			{
				in = new BufferedReader (new FileReader (file));
				b = new StringBuffer ((int) file.length ());
				while ((s = in.readLine ()) != null)
				{
					b.append (s).append ('\n');
				}
				in.close ();
				old = b.toString ();
			}
			else
			{
				b = new StringBuffer ();
				old = null;
			}

			in = new BufferedReader (new FileReader (tmp));
			b.setLength (0);
			while ((s = in.readLine ()) != null)
			{
				b.append (s).append ('\n');
			}
			in.close ();

			if ((old == null) || !old.equals (b.toString ()))
			{
				getLog().info ("-> Updating file " + file);
				BufferedWriter out
					= new BufferedWriter (new FileWriter (file));
				for (i = 0; i < b.length (); i++)
				{
					if (b.charAt (i) == '\n')
					{
						out.newLine ();
					}
					else
					{
						out.write (b.charAt (i));
					}
				}
				out.flush ();
				out.close ();
			}
		}
		catch (Exception e)
		{
			throw new MojoExecutionException
				("Cannot enhance " + file + ", " + e.getMessage (), e);
		}
	}
	
	public void createTmp () throws MojoExecutionException
	{
		
		File tmp = null;
		try {
			tmp = Files.createTempFile("src-enhanced",".tmp").toFile();
		} catch (IOException e) {
			throw new MojoExecutionException("Can't create temp file", e);
		}
		this.tmp = tmp;
	}

	
	public void execute() throws MojoExecutionException {
		Iterator<File> it = getFiles();
		while (it.hasNext()) {
			File file = (File) it.next();
			getLog().info(String.format("processing %s", file.toString()));
			createTmp();
			enhance(file);
		}
		if (stamp != null)
		{
			try
			{
				stamp.createNewFile ();
			}
			catch (IOException e)
			{
				throw new MojoExecutionException
					("Cannot create stamp file " + stamp, e);
			}
			stamp.setLastModified (System.currentTimeMillis ());
		}
    }
}
